<?php

/*
 * This file is part of the slug package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Slug;

use Pressop\Component\Slug\Exception\SlugException;
use Pressop\Component\Slug\Model\HierarchySlugInterface;

/**
 * Interface HierarchySluggerInterface
 *
 * @author Benjamin Georgeault
 */
interface HierarchySluggerInterface
{
    /**
     * @param HierarchySlugInterface $hierarchySlugObject
     * @throws SlugException
     */
    public function generateHierarchySlug(HierarchySlugInterface $hierarchySlugObject);
}
