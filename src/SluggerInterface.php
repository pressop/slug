<?php

/*
 * This file is part of the pressop-slug package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Slug;

use Pressop\Component\Slug\Exception\SlugException;
use Pressop\Component\Slug\Model\SlugInterface;

/**
 * Interface SluggerInterface
 *
 * @author Benjamin Georgeault
 */
interface SluggerInterface
{
    /**
     * @param SlugInterface $slugObject
     * @throws SlugException
     */
    public function generateSlug(SlugInterface $slugObject);
}
