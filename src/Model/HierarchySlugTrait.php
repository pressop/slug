<?php

/*
 * This file is part of the slug package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Slug\Model;

/**
 * Trait HierarchySlugTrait
 *
 * @author Benjamin Georgeault
 * @see HierarchySlugInterface
 * @see \Pressop\Component\Hierarchy\Model\HierarchyInterface
 */
trait HierarchySlugTrait // implements HierarchySlugInterface, \Pressop\Component\Hierarchy\Model\HierarchyInterface
{
    use SlugTrait;

    /**
     * @var string
     */
    protected $hierarchySlug = '';

    /**
     * @inheritdoc
     */
    public function getHierarchySlug(): string
    {
        return $this->hierarchySlug;
    }

    /**
     * @inheritdoc
     */
    public function setHierarchySlug(string $hierarchySlug)
    {
        $this->hierarchySlug = $hierarchySlug;

        return $this;
    }
}
