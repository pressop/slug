<?php

/*
 * This file is part of the pressop-slug package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Slug\Model;

/**
 * Trait SlugTrait
 *
 * @author Benjamin Georgeault
 * @see SlugInterface
 */
trait SlugTrait // implements SlugInterface
{
    /**
     * @var string
     */
    protected $slug = '';

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     * @return $this
     */
    public function setSlug(string $slug)
    {
        $this->slug = $slug;

        return $this;
    }
}
