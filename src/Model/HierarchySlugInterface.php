<?php

/*
 * This file is part of the slug package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Slug\Model;

use Pressop\Component\Hierarchy\Model\HierarchyInterface;

/**
 * Interface HierarchySlugInterface
 *
 * @author Benjamin Georgeault
 * @method HierarchySlugInterface|null getParent()
 * @method HierarchySlugInterface[] getChildren()
 */
interface HierarchySlugInterface extends HierarchyInterface, SlugInterface
{
    /**
     * @return string
     */
    public function getHierarchySlug(): string;

    /**
     * @param string $hierarchySlug
     * @return $this
     */
    public function setHierarchySlug(string $hierarchySlug);
}
