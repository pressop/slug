<?php

/*
 * This file is part of the pressop-slug package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Slug\Bridge\Symfony;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class PressopSlugBundle
 *
 * @author Benjamin Georgeault
 */
class PressopSlugBundle extends Bundle
{
}
