<?php

/*
 * This file is part of the pressop-slug package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Slug\Bridge\Symfony\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/**
 * Class PressopSlugExtension
 *
 * @author Benjamin Georgeault
 */
class PressopSlugExtension extends Extension
{
    /**
     * @inheritDoc
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yaml');

        if (class_exists('Pressop\\Component\\Hierarchy\\Model\\HierarchyInterface')) {
            $loader->load('hierarchy.yaml');
        }
    }

    /**
     * @inheritDoc
     */
    public function getAlias()
    {
        return 'pressop_slug';
    }
}
