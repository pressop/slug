<?php

/*
 * This file is part of the pressop-slug package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Slug;

use Pressop\Component\Inflector\Inflector;
use Pressop\Component\Slug\Exception\SlugException;
use Pressop\Component\Slug\Model\SlugInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;

/**
 * Class Slugger
 *
 * @author Benjamin Georgeault
 */
class Slugger implements SluggerInterface
{
    /**
     * @var string
     */
    private $delimiter;

    /**
     * @var null|PropertyAccessor
     */
    private $propertyAccessor;

    /**
     * Slugger constructor.
     * @param string $delimiter
     */
    public function __construct(string $delimiter = '-')
    {
        $this->delimiter = $delimiter;
    }

    /**
     * @inheritDoc
     */
    public function generateSlug(SlugInterface $slugObject)
    {
        $fields = $slugObject::getSlugFields();

        $propertyAccessor = $this->getPropertyAccessor();

        $values = [];
        foreach ($fields as $field) {
            if (null !== $value = $propertyAccessor->getValue($slugObject, $field)) {
                $values[] = $this->normalize($value);
            }
        }

        if (empty($values)) {
            throw new SlugException(sprintf(
                'All values used to generate "%s"\'s slug are empty ("%s").',
                get_class($slugObject),
                json_encode($fields)
            ));
        }

        $slugObject->setSlug(str_replace('-', $this->delimiter, implode($this->delimiter, $values)));
    }

    /**
     * @return PropertyAccessor
     */
    private function getPropertyAccessor(): PropertyAccessor
    {
        return $this->propertyAccessor ? : $this->propertyAccessor = PropertyAccess::createPropertyAccessor();
    }

    /**
     * @param string $value
     * @return string
     */
    private function normalize(string $value): string
    {
        $value = strtr(
            $value,
            utf8_decode('àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ'),
            'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'
        );
        $value = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $value);
        $value = Inflector::tableize(Inflector::classify($value));

        return $value;
    }
}
