<?php

/*
 * This file is part of the pressop-slug package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Slug\Doctrine\ORM\Subscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Pressop\Component\Slug\Model\SlugInterface;
use Pressop\Component\Slug\Slugger;
use Pressop\Component\Slug\SluggerInterface;

/**
 * Class SlugSubscriber
 *
 * @author Benjamin Georgeault
 */
class SlugSubscriber implements EventSubscriber
{
    /**
     * @var SluggerInterface
     */
    private $slugger;

    /**
     * @var bool
     */
    private $onPreUpdate;

    /**
     * SlugSubscriber constructor.
     * @param SluggerInterface $slugger
     * @param bool $onPreUpdate
     */
    public function __construct(SluggerInterface $slugger = null, bool $onPreUpdate = true)
    {
        $this->slugger = $slugger ? : new Slugger();
        $this->onPreUpdate = $onPreUpdate;
    }

    /**
     * @inheritDoc
     */
    public function getSubscribedEvents()
    {
        $events = [
            Events::prePersist,
        ];

        if ($this->onPreUpdate) {
            $events[] = Events::preUpdate;
        }

        return $events;
    }

    /**
     * @param LifecycleEventArgs $event
     */
    public function prePersist(LifecycleEventArgs $event)
    {
        $this->generateSlug($event);
    }

    /**
     * @param LifecycleEventArgs $event
     */
    public function preUpdate(LifecycleEventArgs $event)
    {
        $this->generateSlug($event);
    }

    /**
     * @param LifecycleEventArgs $event
     */
    private function generateSlug(LifecycleEventArgs $event)
    {
        $entity = $event->getEntity();

        if ($entity instanceof SlugInterface) {
            $this->slugger->generateSlug($entity);
        }
    }
}
