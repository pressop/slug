<?php

/*
 * This file is part of the pressop-slug package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Slug\Doctrine\ORM\Subscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping\ClassMetadata;
use Pressop\Component\Slug\Model\HierarchySlugInterface;
use Pressop\Component\Slug\Model\SlugInterface;

/**
 * Class MappingSubscriber
 *
 * @author Benjamin Georgeault
 */
class MappingSubscriber implements EventSubscriber
{
    /**
     * @inheritDoc
     */
    public function getSubscribedEvents()
    {
        return [
            Events::loadClassMetadata,
        ];
    }

    /**
     * @param LoadClassMetadataEventArgs $eventArgs
     * @throws \Doctrine\ORM\Mapping\MappingException
     */
    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
    {
        $metadata = $eventArgs->getClassMetadata();

        if (null === $metadata->reflClass) {
            return;
        }

        $reflectionClass = $metadata->getReflectionClass();
        if ($reflectionClass->implementsInterface(SlugInterface::class)) {
            $this->mappingSlug($metadata);
        }
        
        if (
            class_exists('Pressop\\Component\\Hierarchy\\Model\\HierarchyInterface') &&
            $reflectionClass->implementsInterface(HierarchySlugInterface::class)
        ) {
            $this->mappingHierarchySlug($metadata);
        }
    }

    /**
     * @param ClassMetadata $metadata
     * @throws \Doctrine\ORM\Mapping\MappingException
     */
    private function mappingSlug(ClassMetadata $metadata)
    {
        if (!$metadata->hasField('slug')) {
            $metadata->mapField([
                'fieldName' => 'slug',
                'type' => 'string',
                'length' => 255,
            ]);
        }

        $metadata->table['indexes']['slug_idx'] = [
            'columns' => ['slug'],
        ];
    }

    /**
     * @param ClassMetadata $metadata
     * @throws \Doctrine\ORM\Mapping\MappingException
     */
    private function mappingHierarchySlug(ClassMetadata $metadata)
    {
        if (!$metadata->hasField('hierarchySlug')) {
            $metadata->mapField([
                'fieldName' => 'hierarchySlug',
                'type' => 'string',
                'length' => 1023,
            ]);
        }

        $metadata->table['indexes']['hierarchy_slug_idx'] = [
            'columns' => ['hierarchySlug'],
        ];
    }
}
