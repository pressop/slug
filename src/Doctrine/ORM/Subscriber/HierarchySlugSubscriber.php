<?php

/*
 * This file is part of the slug package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Slug\Doctrine\ORM\Subscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Pressop\Component\Slug\HierarchySlugger;
use Pressop\Component\Slug\HierarchySluggerInterface;
use Pressop\Component\Slug\Model\HierarchySlugInterface;

/**
 * Class HierarchySlugSubscriber
 *
 * @author Benjamin Georgeault
 */
class HierarchySlugSubscriber implements EventSubscriber
{
    /**
     * @var HierarchySluggerInterface
     */
    private $slugger;

    /**
     * HierarchySlugSubscriber constructor.
     * @param HierarchySluggerInterface|null $slugger
     */
    public function __construct(HierarchySluggerInterface $slugger = null)
    {
        $this->slugger = $slugger ? : new HierarchySlugger();
    }

    /**
     * @inheritDoc
     */
    public function getSubscribedEvents()
    {
        return [
            Events::preFlush,
        ];
    }

    /**
     * @param LifecycleEventArgs $event
     */
    public function preFlush(LifecycleEventArgs $event)
    {
        $this->generateHierarchySlug($event);
    }

    /**
     * @param LifecycleEventArgs $event
     */
    private function generateHierarchySlug(LifecycleEventArgs $event)
    {
        $entity = $event->getEntity();

        if ($entity instanceof HierarchySlugInterface) {
            $this->slugger->generateHierarchySlug($entity);
        }
    }
}
