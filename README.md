Slug
=============

Object slug logic.

## Install

```
composer require pressop/slug
```

## License

This bundle is under the MIT license. See the complete license:

    LICENSE
